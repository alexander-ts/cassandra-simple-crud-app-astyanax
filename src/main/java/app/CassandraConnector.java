package app;

import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import org.springframework.stereotype.Service;

import static com.netflix.astyanax.test.TestConstants.KEYSPACE_NAME;

/**
 * Class used for connecting to Cassandra database.
 */
@Service
public class CassandraConnector {
    /** Cassandra Keyspace. */
    private Keyspace keyspace;

    /** Cassandra AstyanaxContext. */
    private AstyanaxContext<Keyspace> context;

    /**
     * Creates connection to Cassandra specified by provided node IP
     * address and port number.
     *
     * @param node Cluster node IP address.
     * @param port Port of cluster host.
     */
    public void getConnection(final String node, final int port) {
        this.context = new AstyanaxContext.Builder()
                .forCluster("Test Cluster")
                .forKeyspace("cassandra_test")
                .withAstyanaxConfiguration(new AstyanaxConfigurationImpl()
                        .setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE)
                )
                .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("MyConnectionPool")
                        .setPort(port)
                        .setMaxConnsPerHost(1)
                        .setSeeds(node + ":" + port)
                        .setSocketTimeout(60000)
                )
                .withAstyanaxConfiguration(new AstyanaxConfigurationImpl()
                        .setCqlVersion("3.0.0")
                        .setTargetCassandraVersion("1.2")
                )
                .withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
                .buildKeyspace(ThriftFamilyFactory.getInstance());

        this.context.start();
        this.keyspace = context.getClient();
    }

    /**
     * Provide my Keyspace.
     *
     * @return My keyspace.
     */
    public Keyspace getKeyspace() {
        return this.keyspace;
    }

    /** Close cluster. */
    public void close() {
        this.context.shutdown();
    }
}
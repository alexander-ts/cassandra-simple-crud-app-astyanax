package app.controller;

import app.dao.UserPersistence;
import app.model.User;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserPersistence userPersistence;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @CrossOrigin
    @RequestMapping("/users")
    public List<User> getAllUsers() {
        List<User> result = new ArrayList<>();
        List<Optional<User>> optList = userPersistence.queryAllUsers();
        optList.stream().forEach(optUser -> result.add(optUser.get()));
        return result;
    }

    @CrossOrigin
    @RequestMapping("/users/{id}")
    public User getUser(@PathVariable Integer id) {
        Optional<User> optUser = userPersistence.findOne(id);
        if(optUser.isPresent())
            return optUser.get();
        else
            return null;
    }

    @CrossOrigin
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public @ResponseBody String createUser(@RequestBody User user) {
        JSONObject response = new JSONObject();
        if(user.getId() != null) {
            userPersistence.createUser(user.getId(), user.getFname(), user.getLname(), user.getEmail());
            response.put("message", "User created");
            return response.toJSONString();
        } else {
            response.put("message", "Error: User id can't be null!");
            return response.toJSONString();
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public @ResponseBody String updateUser(@RequestBody User user) {
        JSONObject response = new JSONObject();
        if(user.getId() != null) {
            userPersistence.updateUser(user.getId(), user.getFname(), user.getLname(), user.getEmail());
            response.put("message", "User updated");
            return response.toJSONString();
        } else {
            response.put("message", "Error: User id can't be null!");
            return response.toJSONString();
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String deleteUser(@PathVariable Integer id) {
        JSONObject response = new JSONObject();
        userPersistence.deleteUser(id);
        response.put("message", "User deleted");
        return response.toJSONString();
    }
}
package app.dao;

import app.ApplicationProperties;
import app.CassandraConnector;
import app.model.User;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.CqlResult;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.serializers.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static java.lang.System.out;

@Service
@PropertySource("classpath:application.properties")
public class UserPersistence {

    private CassandraConnector client;
    private ApplicationProperties applicationProperties;

    @Autowired
    public UserPersistence(ApplicationProperties applicationProperties, CassandraConnector client) {
        this.client = client;
        this.applicationProperties = applicationProperties;
        out.println("Connecting to IP Address " + applicationProperties.getUrl() + ":" + applicationProperties.getPort() + "...");
        client.getConnection(applicationProperties.getUrl(), applicationProperties.getPort());
    }
    /**
     * Close my underlying Cassandra connection.
     */
    private void close() {
        client.close();
    }


    ColumnFamily<String, String> userColumnFamily =
            new ColumnFamily<String, String>("user",
                    StringSerializer.get(), StringSerializer.get(), StringSerializer.get());


    /**
     * Persist provided user information.
     *
     * @param id ID of user to be persisted.
     * @param fname First Name of user to be persisted.
     * @param lname Last Name of user to be persisted.
     * @param email Email of user to be persisted.
     */
    public void persistUser(final Integer id, final String fname, final String lname, final String email) {
        try {
            OperationResult<CqlResult<String, String>> result
                    = client.getKeyspace().prepareQuery(userColumnFamily)
                    .withCql(String.format(
                            "INSERT INTO cassandra_test.user (userid, user_fname, user_lname, user_email) VALUES (%d, '%s', '%s', '%s');",
                            id, fname, lname, email))
                    .execute();
//            for (Row<String, String> row : result.getResult().getRows()) {
//                System.out.println(row.getColumns().getColumnByName("user_fname"));
//            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create user with provided information.
     *
     * @param id ID of user to be persisted.
     * @param fname First Name of user to be persisted.
     * @param lname Last Name of user to be persisted.
     * @param email Email of user to be persisted.
     */
    public void createUser(final Integer id, final String fname, final String lname, final String email) {
        try {
            OperationResult<CqlResult<String, String>> result
                    = client.getKeyspace().prepareQuery(userColumnFamily)
                    .withCql(String.format(
                            "INSERT INTO cassandra_test.user (userid, user_fname, user_lname, user_email) VALUES (%d, '%s', '%s', '%s') IF NOT EXISTS",
                            id, fname, lname, email))
                    .execute();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update user with provided information.
     *
     * @param id ID of user.
     * @param fname First Name of user to be updated.
     * @param lname Last Name of user to be updated.
     * @param email Email of user to be updated.
     */
    public void updateUser(final Integer id, final String fname, final String lname, final String email) {
        try {
            OperationResult<CqlResult<String, String>> result
                    = client.getKeyspace().prepareQuery(userColumnFamily)
                    .withCql(String.format(
                            "UPDATE cassandra_test.user SET user_fname = '%s', user_lname = '%s', user_email = '%s' WHERE userid = %d IF EXISTS",
                            fname, lname, email, id))
                    .execute();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns user matching provided id.
     *
     * @param userid ID of desired user.
     * @return Desired user if match is found; Optional.empty() if no match is found.
     */
    public Optional<User> findOne(final int userid) {
        Row userRow = null;
        try {
            OperationResult<CqlResult<String, String>> result
                    = client.getKeyspace().prepareQuery(userColumnFamily)
                    .withCql(String.format(
                            "SELECT * from cassandra_test.user WHERE userid = %d;",
                            userid))
                    .execute();
            userRow = result.getResult().getRows().getRowByIndex(0);
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
        final Optional<User> user;
        user = userRow != null
                ? Optional.of(new User(
                userRow.getColumns().getColumnByName("userid").getIntegerValue(),
                userRow.getColumns().getColumnByName("user_fname").getStringValue(),
                userRow.getColumns().getColumnByName("user_lname").getStringValue(),
                userRow.getColumns().getColumnByName("user_email").getStringValue()))
                : Optional.empty();
        return user;
    }

    /**
     * Returns all users.
     *
     * @return Desired user if match is found; Optional.empty() if no match is found.
     */
    public List<Optional<User>> queryAllUsers() {
        List<Optional<User>> users = new ArrayList<>();
        try {
            OperationResult<CqlResult<String, String>> result
                    = client.getKeyspace().prepareQuery(userColumnFamily)
                    .withCql("SELECT * from cassandra_test.user")
                    .execute();
            for (Row<String, String> userRow : result.getResult().getRows()) {
                final Optional<User> user =
                        userRow != null
                                ? Optional.of(new User(
                                userRow.getColumns().getColumnByName("userid").getIntegerValue(),
                                userRow.getColumns().getColumnByName("user_fname").getStringValue(),
                                userRow.getColumns().getColumnByName("user_lname").getStringValue(),
                                userRow.getColumns().getColumnByName("user_email").getStringValue()))
                                : Optional.empty();
                users.add(user);
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }

        return users;
    }

    /**
     * Deletes the user with the provided id.
     *
     * @param id ID of user to be deleted.
     */
    public void deleteUser(final Integer id) {
        try {
            OperationResult<CqlResult<String, String>> result
                    = client.getKeyspace().prepareQuery(userColumnFamily)
                    .withCql(String.format("DELETE FROM cassandra_test.user WHERE userid = %d;", id))
                    .execute();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }
}
